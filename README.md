vagrant mh-proto
================

`this repo is a public sample of DCE vagrantfiles for various . it's not being
updated nor maintained; please do not fork.`

proto for vagrant instances

use
---

1. install [vagrant][vagrant] and [vmware plugin][vmware_plugin]

2. clone vagrant proto repo

    $> git clone git@bitbucket.org:hudcede/vagrant-sample.git

3. ways to set up ssh keys:

    i. use the insecure key provided:

        $> cp mh-vagrant-proto/shared/ansible_vagrant_insecure_key ~/.ssh

    ii. create your own ssh keys (in macos):

        $> ssh-keygen
          Generating public/private rsa key pair.
          Enter file in which to save the key (/Users/nmaekawa/.ssh/id_rsa): myownkeys_rsa
          Enter passphrase (empty for no passphrase):
          Enter same passphrase again:
          Your identification has been saved in myownkeys_rsa.
          Your public key has been saved in myownkeys_rsa.pub.
          The key fingerprint is:
          b5:18:54:ae:09:ae:96:bc:24:18:c4:42:72:36:fc:a9
          nmaekawa@hypnotoad.disted.harvard.edu
          The key's randomart image is:
          +--[ RSA 2048]----+
          | .o+     ...  |
          | +o..   . .   |
          | .o . .. . o  |
          | o   o. . * . |
          | .  .  . S .  |
          | oE. o        |
          | . . *        |
          | + .          |
          | .            |
          +-----------------+

        # edit install_ansible_user.sh to copy your key to the vagrant instance
        $> cat mh-vagrant-proto/install_ansible_user.sh | sed    \
            's/ansible_vagrant_insecure_key.pub/myownkeys_rsa/g' > ./install_ansible_user.sh
        $> mv ./install_ansible_user.sh mh-vagrant-proto/install_ansible_user.sh


    iii. you can use .ssh/config, if you know how to.


4. register a basic centos box, available in a shared disk

        # smb://dev-nfs.fake.domain/vagrant_boxes/centos-6.5-vagrant-mh.box
        $> vagrant box add centos-6.5 centos-6.5-x86_64.vmware.box


5. start a single vagrant instance:

        $> cd mh-vagrant-proto
        $> cp vagrant-single Vagrantfile
        $> vagrant up --provider vmware_fusion

6. some other vagrant files are provided for a simple matterhorn installation
   (vagrant-mh-simple) and a matterhorn installation with multiple nodes
   (vagrant-mh-multi)

to provision
------------

check the provision repo [https://bitbucket.org/hudcede/provision][provision_repo]

>
> IMPORTANT: if you want to save your customizations, fork the mh-vagrant-proto repo
> and push your changes to your fork.
>
> this repo is meant to be a building block; any change goes in your fork!
>

[vagrant]: http://www.vagrantup.com/downloads
[vmware_plugin]: http://www.vagrantup.com/vmware
[provision_repo]: https://bitbucket.org/hudcede/provision


