#!/bin/bash

# create ansible user
/usr/sbin/groupadd --gid 2121 ansible
/usr/sbin/useradd --uid 2121 -g ansible --home /home/ansible --create-home ansible

# change authorized_keys with your own public key!
/bin/mkdir -m 0700 /home/ansible/.ssh
cat /vagrant/shared/ansible_vagrant_insecure_key.pub >> /home/ansible/.ssh/authorized_keys
/bin/chmod 600 /home/ansible/.ssh/authorized_keys
chown -R ansible:ansible /home/ansible/.ssh

if [ $1 ]; then
    /usr/sbin/usermod --password $1 ansible
else
    # default is literally garbage
    /usr/sbin/usermod --password abcdefghijklmn ansible
fi

# add ansible to sudoers
echo "ansible        ALL=(ALL)      NOPASSWD: ALL" >> /etc/sudoers

